
/**
 * Escreva a descrição da classe ImóvelAntigo aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class ImóvelAntigo extends Imóvel
{
    private String endereco;
    private int preco, percentagemvenda, desconto;
    private boolean antigo;
    
    public ImóvelAntigo()
    {
        endereco="";
        preco=0;
        percentagemvenda=0;
        antigo=false;
    }
    public int Desconto()
    {
        if (antigo==true)
        {
            return desconto;
        }
        else
        {
            return 0;
        }
    }
    
    public void setdesconto (int desconto)
    {
        this.desconto=desconto;
    }
    public int getdesconto()
    {
        return desconto;
    }
    public void setantigo (boolean antigo)
    {
        this.antigo=antigo;
    }
    public boolean getantigo()
    {
        return antigo;
    }
}
