
/**
 * Escreva a descrição da classe Imóvel aqui.
 * 
 * @author (seu nome) 
 * @version (número de versão ou data)
 */
public class Imóvel
{
    private String endereco;
    private int preco, percentagemvenda;
    
    public Imóvel()
    {
        endereco="";
        preco=0;
        percentagemvenda=0;
    }
    public int PercentagemVenda()
    {
        return preco-percentagemvenda;
    }
    
    public void setendereco (String endereco)
    {
        this.endereco=endereco;
    }
    public String getendereco()
    {
        return endereco;
    }
    public void setpreco (int preco)
    {
        this.preco=preco;
    }
    public int getpreco()
    {
        return preco;
    }
    public void setpercentagemvenda(int percentagem)
    {
        percentagem=percentagemvenda;
    }
    public int getpercentagemvenda()
    {
        return percentagemvenda;
    }
}
